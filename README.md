Setup instructions
------------------
1. Get vim config from repo and redirect ~/.vimrc:

        $ cd
        $ git clone git@bitbucket.org:akxlr/vimconf2.git .vim
        $ echo "runtime vimrc" > .vimrc

1. Install Vundle:

        $ git clone http://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim

1. Open `vim` (ignore errors) and run `:PluginInstall` to make Vundle get all plugins listed in vimrc (they aren't tracked in this repo). If ANU labs don't let the https traffic through (some proxy issue) then `vim ~/.vim/vimrc` and add `git@github.com:` prefix to each plugin reference. See http://stackoverflow.com/a/21178743/692456.

1. Remap caps lock to escape in Ubuntu - add the following command to startup items:

        xmodmap -e 'clear Lock' -e 'keycode 0x42 = Escape'

   (or in OSX follow http://stackoverflow.com/questions/127591/using-caps-lock-as-esc-in-mac-os-x).

1. Set terminal font:

        $ sudo apt-get install ttf-inconsolata
        $ sudo fc-cache -f -v
        (Then set font to `inconsolata` in terminal options)

   If this doesn't look right in powerline (status bar), might need to use the
   patched version of the font which is downloadable from powerline github - see
   powerline docs. I found that if it doesn't look right immediately it
   eventually seems to fix itself without the patched version though. If using wget
   make sure to use the RAW url.

1. To make solarized colorscheme work, set terminal colours using `./gnome-terminal-colors.sh`.

   In OSX: Download as ZIP https://github.com/tomislav/osx-terminal.app-colors-solarized, set
   .terminal file as default in terminal settings.

Everything should now work. To install new plugins, don't git clone anything -
just add the reference in Vundle section of vimrc and run :PluginInstall